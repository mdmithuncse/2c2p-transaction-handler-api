﻿using Microsoft.AspNetCore.Http;
using System.Linq;
using ToCToP.TransactionHandler.Commons;

namespace ToCToP.TransactionHandler.Business.Extensions
{
    public static class FileExtension
    {
        public static string GetFileType(this IFormFile file)
        {
            return file.FileName.Contains(Enums.FileType.CSV) == true ? Enums.FileType.CSV : Enums.FileType.XML;
        }

        public static bool IsValidFileType(this IFormFile file)
        {
            if (file.FileName.Split('.').Last().Equals(Enums.FileType.CSV))
                return true;

            if (file.FileName.Split('.').Last().Equals(Enums.FileType.XML))
                return true;

            return false;
        }

        public static bool IsValidFileSize(this IFormFile file)
        {
            return file.Length <= Enums.FileSize.ONE_MB
                ? true
                : false;
        }
    }
}
