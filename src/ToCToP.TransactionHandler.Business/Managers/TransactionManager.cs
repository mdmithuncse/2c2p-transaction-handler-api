﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using ToCToP.TransactionHandler.Business.Extensions;
using ToCToP.TransactionHandler.Commons;
using ToCToP.TransactionHandler.Commons.Exceptions;
using ToCToP.TransactionHandler.DataAccess.Repositories.TransactionRepository;
using ToCToP.TransactionHandler.Helpers;
using ToCToP.TransactionHandler.Models.Transaction.Input;
using ToCToP.TransactionHandler.Models.Transaction.Maps;
using ToCToP.TransactionHandler.Models.Transaction.ViewModels;
using ToCToP.TransactionHandler.Commons.Constants;

namespace ToCToP.TransactionHandler.Business.Managers
{
    public interface ITransactionManager
    {
        Task<List<TransactionDto>> GetTransactionsAsync();
        Task<List<TransactionDto>> GetTransactionsAsync(string status);
        Task<List<TransactionDto>> GetTransactionsAsync(string status,
                                                        string currencyCode);
        Task<List<TransactionDto>> GetTransactionsAsync(string status,
                                                        string currencyCode,
                                                        DateTime fromDate,
                                                        DateTime toDate);
        Task ImportTransactionsAsync(IFormFile file);
    }

    public class TransactionManager : ITransactionManager
    {
        private readonly IMapper _mapper;
        private readonly ILogger<TransactionManager> _logger;
        private readonly ICustomTransactionRepository _customTransactionRepository;
        private readonly ICsvFileBuilder _csvFileBuilder;

        public TransactionManager(IMapper mapper,
                                  ILogger<TransactionManager> logger,
                                  ICustomTransactionRepository customTransactionRepository,
                                  ICsvFileBuilder csvFileBuilder)
        {
            _mapper = mapper;
            _logger = logger;
            _customTransactionRepository = customTransactionRepository;
            _csvFileBuilder = csvFileBuilder;
        }

        private async Task<List<ApplicationError>> ValidateFileAsync(IFormFile file)
        {
            var applicationError = new List<ApplicationError>();

            if (!FileExtension.IsValidFileType(file))
                applicationError.Add(new ApplicationError
                {
                    ErrorCode = ErrorCodes.INVALID_FILE_FORMAT,
                    Field = "file",
                    Message = "unknown file format"
                });

            if (!FileExtension.IsValidFileSize(file))
                applicationError.Add(new ApplicationError
                {
                    ErrorCode = ErrorCodes.INVALID_FILE_SIZE,
                    Field = "file",
                    Message = "invalid file size"
                });

            return applicationError.ToList();
        }

        private async Task ImportCsvFileTransactionsAsync(IFormFile file)
        {
            var importDatas = _csvFileBuilder.Read<ImportTransactionCsvRequest, ImportTransactionCsvRequestMap>(file.OpenReadStream()).ToList();

            if (!importDatas.Any())
            {
                return;
            }

            await _customTransactionRepository.ImportTransactionsAsync(importDatas);
        }

        public async Task<List<TransactionDto>> GetTransactionsAsync()
        {
            var items = await _customTransactionRepository.GetTransactionsAsync();

            return _mapper.Map<List<TransactionDto>>(items);
        }

        public async Task<List<TransactionDto>> GetTransactionsAsync(string status)
        {
            var items = await _customTransactionRepository.GetTransactionsAsync(status);

            return _mapper.Map<List<TransactionDto>>(items);
        }

        public async Task<List<TransactionDto>> GetTransactionsAsync(string status,
                                                                     string currencyCode)
        {
            var items = await _customTransactionRepository.GetTransactionsAsync(status, currencyCode);

            return _mapper.Map<List<TransactionDto>>(items);
        }

        public async Task<List<TransactionDto>> GetTransactionsAsync(string status,
                                                                     string currencyCode,
                                                                     DateTime fromDate,
                                                                     DateTime toDate)
        {
            var items = await _customTransactionRepository.GetTransactionsAsync(status,
                                                                                currencyCode,
                                                                                fromDate,
                                                                                toDate);

            return _mapper.Map<List<TransactionDto>>(items);
        }

        public async Task ImportTransactionsAsync(IFormFile file)
        {
            var errors = await ValidateFileAsync(file);

            if (errors.Any())
                throw new ApiApplicationException((int) HttpStatusCode.BadRequest, ErrorCodes.BAD_REQUEST, "", "file", errors);

            var fileType = FileExtension.GetFileType(file);

            if (fileType == Enums.FileType.CSV)
            {
                await ImportCsvFileTransactionsAsync(file);
            }

            else
            {
                // ToDo: Call Xml File Import Method
            }
        }
    }
}
