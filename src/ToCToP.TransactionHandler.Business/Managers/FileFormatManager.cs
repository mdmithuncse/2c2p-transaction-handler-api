﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;
using ToCToP.TransactionHandler.DataAccess.UnitOfWorks;
using ToCToP.TransactionHandler.Entities;
using ToCToP.TransactionHandler.Models.FileFormats.ViewModels;

namespace ToCToP.TransactionHandler.Business.Managers
{
    public interface IFileFormatManager
    {
        Task<List<FileFormatDto>> GetFileFormatsAsync();
    }

    public class FileFormatManager : IFileFormatManager
    {
        private readonly IMapper _mapper;
        private readonly ILogger<FileFormatManager> _logger;
        private readonly IUnitOfWork _unitOfWork;

        public FileFormatManager(IMapper mapper,
                                 ILogger<FileFormatManager> logger,
                                 IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _logger = logger;
            _unitOfWork = unitOfWork;
        }

        public async Task<List<FileFormatDto>> GetFileFormatsAsync()
        {
            var items = await _unitOfWork.GetRepository<FileFormat>().GetAllAsync();

            return _mapper.Map<List<FileFormatDto>>(items);
        }
    }
}
