﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;
using ToCToP.TransactionHandler.DataAccess.UnitOfWorks;
using ToCToP.TransactionHandler.Entities;
using ToCToP.TransactionHandler.Models.TransactionStatus.ViewModels;

namespace ToCToP.TransactionHandler.Business.Managers
{
    public interface ITransactionStatusManager
    {
        Task<List<TransactionStatusDto>> GetTransactionStatusesAsync();
    }
    public class TransactionStatusManager : ITransactionStatusManager
    {
        private readonly IMapper _mapper;
        private readonly ILogger<TransactionStatusManager> _logger;
        private readonly IUnitOfWork _unitOfWork;

        public TransactionStatusManager(IMapper mapper,
                                 ILogger<TransactionStatusManager> logger,
                                 IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _logger = logger;
            _unitOfWork = unitOfWork;
        }

        public async Task<List<TransactionStatusDto>> GetTransactionStatusesAsync()
        {
            var items = await _unitOfWork.GetRepository<TransactionStatus>().GetAllAsync();

            return _mapper.Map<List<TransactionStatusDto>>(items);
        }
    }
}
