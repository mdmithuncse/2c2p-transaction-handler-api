﻿using Microsoft.Extensions.Caching.Memory;
using System;

namespace ToCToP.TransactionHandler.Caching
{
    public class CacheService
    {
        private readonly IMemoryCache _cache;
        private readonly MemoryCacheEntryOptions _cacheExpirationOptions;

        public CacheService() { }

        public CacheService(IMemoryCache cache)
        {
            _cache = cache;
            _cacheExpirationOptions = new MemoryCacheEntryOptions
            {
                AbsoluteExpiration = DateTime.Now.AddMinutes(30),
                Priority = CacheItemPriority.Normal
            };
        }

        public virtual bool IsExists(string key)
        {
            return _cache.TryGetValue(key, out var result);
        }

        public virtual T Get<T>(string key)
        {
            _cache.TryGetValue<T>(key, out var result);
            return result;
        }

        public virtual void Set<T>(string key, T value, MemoryCacheEntryOptions options = null)
        {
            var cacheOptions = options ?? _cacheExpirationOptions;
            _cache.Set(key, value, cacheOptions);
        }

        public void Remove(string key)
        {
            _cache.Remove(key);
        }
    }
}
