﻿using CsvHelper;
using CsvHelper.Configuration;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace ToCToP.TransactionHandler.Helpers
{
    public interface ICsvFileBuilder
    {
        byte[] Build<T, TP>(List<T> records) where TP : ClassMap;
        byte[] Build<T>(List<T> records);
        IEnumerable<T> Read<T, TP>(Stream stream) where TP : ClassMap;
        IEnumerable<T> Read<T>(Stream stream);
    }

    public class CsvFileBuilder : ICsvFileBuilder
    {
        public byte[] Build<T, TP>(List<T> records) where TP : ClassMap
        {
            using var memoryStream = new MemoryStream();
            using (var streamWriter = new StreamWriter(memoryStream))
            {
                using var csvWriter = new CsvWriter(streamWriter, CultureInfo.InvariantCulture);
                csvWriter.Configuration.RegisterClassMap<TP>();
                csvWriter.WriteRecords(records);
            }

            return memoryStream.ToArray();
        }

        public byte[] Build<T>(List<T> records)
        {
            using var memoryStream = new MemoryStream();
            using (var streamWriter = new StreamWriter(memoryStream))
            {
                using var csvWriter = new CsvWriter(streamWriter, CultureInfo.InvariantCulture);
                csvWriter.WriteRecords(records);
            }

            return memoryStream.ToArray();
        }

        public IEnumerable<T> Read<T, TP>(Stream stream) where TP : ClassMap
        {
            using var reader = new StreamReader(stream);
            using var csv = new CsvReader(reader, CultureInfo.InvariantCulture);
            csv.Configuration.RegisterClassMap<TP>();
            return csv.GetRecords<T>().ToList();
        }

        public IEnumerable<T> Read<T>(Stream stream)
        {
            using var reader = new StreamReader(stream);
            using var csv = new CsvReader(reader, CultureInfo.InvariantCulture);
            return csv.GetRecords<T>().ToList();
        }
    }
}
