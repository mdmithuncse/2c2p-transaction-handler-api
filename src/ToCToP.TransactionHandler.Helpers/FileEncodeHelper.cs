﻿using System;

namespace ToCToP.TransactionHandler.Helpers
{
    public static class FileEncodeHelper
    {
        public static string ConvertByteArrayToBase64DataUri(byte[] source, string contentType)
        {
            return $"data:{contentType};base64,{Convert.ToBase64String(source)}";
        }
    }
}
