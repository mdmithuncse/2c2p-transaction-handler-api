﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using ToCToP.TransactionHandler.Commons.Extensions;

namespace ToCToP.TransactionHandler.DataAccess.Context
{
    public static class ApplicationDbContextExtension
    {
        public static async Task DbMigrateAsync(this ApplicationDbContext context)
        {
            var migrations = await context.Database.GetPendingMigrationsAsync();

            if (EnumerableExtensions.IsAny(migrations))
                await context.Database.MigrateAsync();
        }
    }
}
