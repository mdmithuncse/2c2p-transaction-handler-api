﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Logging;
using System;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using ToCToP.TransactionHandler.Entities;

namespace ToCToP.TransactionHandler.DataAccess.Context
{
    public class ApplicationDbContext : DbContext
    {
        private readonly ILogger<ApplicationDbContext> _logger;

        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
            try
            {
                _logger = this.GetService<ILogger<ApplicationDbContext>>();
            }
            catch (Exception)
            {
                // ignored
            }
        }

        internal ApplicationDbContext(DbContextOptions options, ILogger<ApplicationDbContext> logger) : base(options)
        {
            _logger = logger;
        }

        public DbSet<FileFormat> FileFormats { get; set; }
        public DbSet<TransactionStatus> TransactionStatus { get; set; }
        public DbSet<Transaction> Transactions { get; set; }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            try
            {
                return await base.SaveChangesAsync(cancellationToken);
            }
            catch (Exception sqlException)
            {
                _logger.LogError(sqlException.HResult, sqlException, $"Sql exception: {sqlException.Message}");
                throw sqlException;
            }
        }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

            base.OnModelCreating(builder);
        }
    }
}
