﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToCToP.TransactionHandler.Commons;
using ToCToP.TransactionHandler.Commons.Constants;
using ToCToP.TransactionHandler.DataAccess.Context;
using ToCToP.TransactionHandler.DataAccess.UnitOfWorks;
using ToCToP.TransactionHandler.Entities;
using ToCToP.TransactionHandler.Models.Transaction.Input;
using ToCToP.TransactionHandler.Models.Transaction.ViewModels;

namespace ToCToP.TransactionHandler.DataAccess.Repositories.TransactionRepository
{
    public interface ICustomTransactionRepository
    {
        Task<List<TransactionDto>> GetTransactionsAsync();
        Task<List<TransactionDto>> GetTransactionsAsync(string status);
        Task<List<TransactionDto>> GetTransactionsAsync(string status,
                                                        string currencyCode);
        Task<List<TransactionDto>> GetTransactionsAsync(string status,
                                                        string currencyCode,
                                                        DateTime fromDate,
                                                        DateTime toDate);
        Task ImportTransactionsAsync(List<ImportTransactionCsvRequest> importDatas);
    }

    public class CustomTransactionRepository : Repository<Transaction>, IRepository<Transaction>, ICustomTransactionRepository
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public CustomTransactionRepository(ApplicationDbContext dbContext,
                                           IMapper mapper,
                                           IUnitOfWork unitOfWork) : base(dbContext)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<List<TransactionDto>> GetTransactionsAsync()
        {
            var items =
                await _unitOfWork.GetRepository<Transaction>(hasCustomRepository: true)
                                 .GetAllAsync(include:
                                                  source =>
                                                  source.Include(include => include.FileFormat)
                                                        .Include(include => include.TransactionStatus));

            return _mapper.Map<List<TransactionDto>>(items);
        }

        public async Task<List<TransactionDto>> GetTransactionsAsync(string status)
        {
            var getStatus =
                await _unitOfWork.GetRepository<TransactionStatus>()
                                 .GetFirstOrDefaultAsync(predicate: x => x.Status.Contains(status));

            var items = await _unitOfWork.GetRepository<Transaction>(hasCustomRepository: true)
                                         .GetAllAsync(include:
                                                          source =>
                                                          source.Include(include => include.FileFormat)
                                                                .Include(include => include.TransactionStatus),
                                                      predicate: x => x.TransactionStatusId == getStatus.Id);

            return _mapper.Map<List<TransactionDto>>(items);
        }

        public async Task<List<TransactionDto>> GetTransactionsAsync(string status,
                                                                     string currencyCode)
        {
            var getStatus =
                await _unitOfWork.GetRepository<TransactionStatus>()
                                 .GetFirstOrDefaultAsync(predicate: x => x.Status.Contains(status));

            var items = await _unitOfWork.GetRepository<Transaction>(hasCustomRepository: true)
                                         .GetAllAsync(include:
                                                          source =>
                                                          source.Include(include => include.FileFormat)
                                                                .Include(include => include.TransactionStatus),
                                                      predicate: x => x.CurrencyCode.Contains(currencyCode) &&
                                                                      x.TransactionStatusId == getStatus.Id);

            return _mapper.Map<List<TransactionDto>>(items);
        }

        public async Task<List<TransactionDto>> GetTransactionsAsync(string status,
                                                                     string currencyCode,
                                                                     DateTime fromDate,
                                                                     DateTime toDate)
        {
            var getStatus =
                await _unitOfWork.GetRepository<TransactionStatus>()
                                 .GetFirstOrDefaultAsync(predicate: x => x.Status.Contains(status));

            var items = await _unitOfWork.GetRepository<Transaction>(hasCustomRepository: true)
                                         .GetAllAsync(include:
                                                          source =>
                                                          source.Include(include => include.FileFormat)
                                                                .Include(include => include.TransactionStatus),
                                                      predicate: x => x.CurrencyCode.Contains(currencyCode) &&
                                                                      x.TransactionStatusId == getStatus.Id &&
                                                                      (x.Date >= fromDate && x.Date <= toDate));

            return _mapper.Map<List<TransactionDto>>(items);
        }

        public async Task ImportTransactionsAsync(List<ImportTransactionCsvRequest> importDatas)
        {
            var statusIds = _unitOfWork.GetRepository<TransactionStatus>().GetAll().ToList();

            var repo = _unitOfWork.GetRepository<Transaction>();
            repo.Insert(importDatas.Select(c => new Transaction
            {
                Created = DateTime.Now,
                TransactionId = c.TransactionId,
                Amount = c.Amount,
                CurrencyCode = c.CurrencyCode,
                Date = DateTime.ParseExact(c.Date, CsvFile.DateFormats.DD_MM_YYYY_HH_MM_SS, null),
                TransactionStatusId = statusIds.Where(x => x.Status == c.Status).Select(x => x.Id).First(),
                FileFormatId = (long)Enums.FileFormat.CSV
            }));

            await _unitOfWork.SaveChangesAsync();
        }
    }
}
