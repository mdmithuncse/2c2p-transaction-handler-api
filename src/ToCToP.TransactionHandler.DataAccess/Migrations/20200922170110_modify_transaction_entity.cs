﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ToCToP.TransactionHandler.DataAccess.Migrations
{
    public partial class modify_transaction_entity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "TransactionStatusId",
                table: "Transactions",
                nullable: false,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "FileFormatId",
                table: "Transactions",
                nullable: false,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "FileFormats",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created",
                value: new DateTime(2020, 9, 23, 0, 1, 10, 37, DateTimeKind.Local).AddTicks(7248));

            migrationBuilder.UpdateData(
                table: "FileFormats",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created",
                value: new DateTime(2020, 9, 23, 0, 1, 10, 38, DateTimeKind.Local).AddTicks(6040));

            migrationBuilder.UpdateData(
                table: "TransactionStatus",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created",
                value: new DateTime(2020, 9, 23, 0, 1, 10, 51, DateTimeKind.Local).AddTicks(1697));

            migrationBuilder.UpdateData(
                table: "TransactionStatus",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created",
                value: new DateTime(2020, 9, 23, 0, 1, 10, 51, DateTimeKind.Local).AddTicks(1735));

            migrationBuilder.UpdateData(
                table: "TransactionStatus",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created",
                value: new DateTime(2020, 9, 23, 0, 1, 10, 51, DateTimeKind.Local).AddTicks(1737));

            migrationBuilder.UpdateData(
                table: "TransactionStatus",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created",
                value: new DateTime(2020, 9, 23, 0, 1, 10, 51, DateTimeKind.Local).AddTicks(1738));

            migrationBuilder.UpdateData(
                table: "TransactionStatus",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created",
                value: new DateTime(2020, 9, 23, 0, 1, 10, 51, DateTimeKind.Local).AddTicks(1739));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "TransactionStatusId",
                table: "Transactions",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AlterColumn<long>(
                name: "FileFormatId",
                table: "Transactions",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.UpdateData(
                table: "FileFormats",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created",
                value: new DateTime(2020, 9, 21, 22, 38, 58, 417, DateTimeKind.Local).AddTicks(8976));

            migrationBuilder.UpdateData(
                table: "FileFormats",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created",
                value: new DateTime(2020, 9, 21, 22, 38, 58, 419, DateTimeKind.Local).AddTicks(7420));

            migrationBuilder.UpdateData(
                table: "TransactionStatus",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created",
                value: new DateTime(2020, 9, 21, 22, 38, 58, 443, DateTimeKind.Local).AddTicks(8497));

            migrationBuilder.UpdateData(
                table: "TransactionStatus",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created",
                value: new DateTime(2020, 9, 21, 22, 38, 58, 443, DateTimeKind.Local).AddTicks(8577));

            migrationBuilder.UpdateData(
                table: "TransactionStatus",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created",
                value: new DateTime(2020, 9, 21, 22, 38, 58, 443, DateTimeKind.Local).AddTicks(8579));

            migrationBuilder.UpdateData(
                table: "TransactionStatus",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created",
                value: new DateTime(2020, 9, 21, 22, 38, 58, 443, DateTimeKind.Local).AddTicks(8581));

            migrationBuilder.UpdateData(
                table: "TransactionStatus",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created",
                value: new DateTime(2020, 9, 21, 22, 38, 58, 443, DateTimeKind.Local).AddTicks(8583));
        }
    }
}
