﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ToCToP.TransactionHandler.DataAccess.Migrations
{
    public partial class initial_migration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "FileFormats",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Created = table.Column<DateTime>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 200, nullable: true),
                    UpdatedBy = table.Column<string>(maxLength: 200, nullable: true),
                    FileType = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FileFormats", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TransactionStatus",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Created = table.Column<DateTime>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 200, nullable: true),
                    UpdatedBy = table.Column<string>(maxLength: 200, nullable: true),
                    Status = table.Column<string>(maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransactionStatus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Transactions",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Created = table.Column<DateTime>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 200, nullable: true),
                    UpdatedBy = table.Column<string>(maxLength: 200, nullable: true),
                    TransactionId = table.Column<string>(maxLength: 50, nullable: false),
                    Amount = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    CurrencyCode = table.Column<string>(maxLength: 3, nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    TransactionStatusId = table.Column<long>(nullable: true),
                    FileFormatId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Transactions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Transactions_FileFormats_FileFormatId",
                        column: x => x.FileFormatId,
                        principalTable: "FileFormats",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_Transactions_TransactionStatus_TransactionStatusId",
                        column: x => x.TransactionStatusId,
                        principalTable: "TransactionStatus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.InsertData(
                table: "FileFormats",
                columns: new[] { "Id", "Created", "CreatedBy", "FileType", "Updated", "UpdatedBy" },
                values: new object[,]
                {
                    { 1L, new DateTime(2020, 9, 21, 22, 38, 58, 417, DateTimeKind.Local).AddTicks(8976), null, "csv", null, null },
                    { 2L, new DateTime(2020, 9, 21, 22, 38, 58, 419, DateTimeKind.Local).AddTicks(7420), null, "xml", null, null }
                });

            migrationBuilder.InsertData(
                table: "TransactionStatus",
                columns: new[] { "Id", "Created", "CreatedBy", "Status", "Updated", "UpdatedBy" },
                values: new object[,]
                {
                    { 1L, new DateTime(2020, 9, 21, 22, 38, 58, 443, DateTimeKind.Local).AddTicks(8497), null, "Approved", null, null },
                    { 2L, new DateTime(2020, 9, 21, 22, 38, 58, 443, DateTimeKind.Local).AddTicks(8577), null, "Failed", null, null },
                    { 3L, new DateTime(2020, 9, 21, 22, 38, 58, 443, DateTimeKind.Local).AddTicks(8579), null, "Finished", null, null },
                    { 4L, new DateTime(2020, 9, 21, 22, 38, 58, 443, DateTimeKind.Local).AddTicks(8581), null, "Rejected", null, null },
                    { 5L, new DateTime(2020, 9, 21, 22, 38, 58, 443, DateTimeKind.Local).AddTicks(8583), null, "Done", null, null }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_FileFormatId",
                table: "Transactions",
                column: "FileFormatId");

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_TransactionStatusId",
                table: "Transactions",
                column: "TransactionStatusId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Transactions");

            migrationBuilder.DropTable(
                name: "FileFormats");

            migrationBuilder.DropTable(
                name: "TransactionStatus");
        }
    }
}
