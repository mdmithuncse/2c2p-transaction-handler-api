﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ToCToP.TransactionHandler.Entities;

namespace ToCToP.TransactionHandler.DataAccess.Configurations
{
    public class TransactionConfiguration : BaseEntityConfiguration<Transaction>
    {
        public override void Configure(EntityTypeBuilder<Transaction> builder)
        {
            builder.Property(t => t.TransactionId).HasMaxLength(50).IsRequired();

            builder.Property(t => t.Amount).HasColumnType("decimal(18, 2)").IsRequired();

            builder.Property(t => t.CurrencyCode).HasMaxLength(3).IsRequired();

            builder.Property(t => t.Date).IsRequired();

            builder.HasOne(t => t.FileFormat).WithMany(t => t.Transactions).OnDelete(DeleteBehavior.SetNull);

            builder.HasOne(t => t.TransactionStatus).WithMany(t => t.Transactions).OnDelete(DeleteBehavior.SetNull);

            builder.Metadata.FindNavigation(nameof(Transaction.FileFormat)).SetPropertyAccessMode(PropertyAccessMode.Field);

            builder.Metadata.FindNavigation(nameof(Transaction.TransactionStatus)).SetPropertyAccessMode(PropertyAccessMode.Field);

            base.Configure(builder);
        }
    }
}
