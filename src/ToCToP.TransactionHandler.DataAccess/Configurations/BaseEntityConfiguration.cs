﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ToCToP.TransactionHandler.Entities;

namespace ToCToP.TransactionHandler.DataAccess.Configurations
{
    public abstract class BaseEntityConfiguration<T> : IEntityTypeConfiguration<T> where T : BaseEntity
    {
        public virtual void Configure(EntityTypeBuilder<T> builder)
        {
            builder.Property(t => t.Id).UseIdentityColumn().IsRequired();

            builder.Property(t => t.Created).HasColumnName("Created").IsRequired();

            builder.Property(t => t.Updated).HasColumnName("Updated");

            builder.Property(t => t.CreatedBy).HasColumnName("CreatedBy").HasMaxLength(200);

            builder.Property(t => t.UpdatedBy).HasColumnName("UpdatedBy").HasMaxLength(200);
        }
    }
}
