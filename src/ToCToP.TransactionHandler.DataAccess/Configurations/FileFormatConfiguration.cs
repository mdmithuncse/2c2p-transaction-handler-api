﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using ToCToP.TransactionHandler.Commons;
using ToCToP.TransactionHandler.Entities;

namespace ToCToP.TransactionHandler.DataAccess.Configurations
{
    public class FileFormatConfiguration : BaseEntityConfiguration<FileFormat>
    {
        public override void Configure(EntityTypeBuilder<FileFormat> builder)
        {
            builder.Property(t => t.FileType).HasMaxLength(50).IsRequired();
            builder.HasData(new FileFormat()
                            {
                                Id = 1,
                                FileType = Enums.FileType.CSV,
                                Created = DateTime.Now
                            }, 
                            new FileFormat()
                            {
                                Id = 2,
                                FileType = Enums.FileType.XML,
                                Created = DateTime.Now
                            });;

            base.Configure(builder);
        }
    }
}
