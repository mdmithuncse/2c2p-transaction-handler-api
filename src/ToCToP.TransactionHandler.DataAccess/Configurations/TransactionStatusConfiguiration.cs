﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using ToCToP.TransactionHandler.Commons;
using ToCToP.TransactionHandler.Entities;

namespace ToCToP.TransactionHandler.DataAccess.Configurations
{
    public class TransactionStatusConfiguiration : BaseEntityConfiguration<TransactionStatus>
    {
        public override void Configure(EntityTypeBuilder<TransactionStatus> builder)
        {
            builder.Property(t => t.Status).HasMaxLength(30).IsRequired();
            builder.HasData(new TransactionStatus()
                            {
                                Id = 1,
                                Status = Enums.TransactionStatus.APPROVED,
                                Created = DateTime.Now
                            },
                            new TransactionStatus()
                            {
                                Id = 2,
                                Status = Enums.TransactionStatus.FAILED,
                                Created = DateTime.Now
                            },
                            new TransactionStatus()
                            {
                                Id = 3,
                                Status = Enums.TransactionStatus.FINISHED,
                                Created = DateTime.Now
                            },
                            new TransactionStatus()
                            {
                                Id = 4,
                                Status = Enums.TransactionStatus.REJECTED,
                                Created = DateTime.Now
                            },new TransactionStatus()
                            {
                                Id = 5,
                                Status = Enums.TransactionStatus.DONE,
                                Created = DateTime.Now
                            });

            base.Configure(builder);
        }
    }
}
