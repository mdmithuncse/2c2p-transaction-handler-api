﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using ToCToP.TransactionHandler.Business.Managers;
using ToCToP.TransactionHandler.Models.TransactionStatus.ViewModels;

namespace ToCToP.TransactionHandler.Api.Controllers
{
    public class TransactionStatusController : BaseController
    {
        private readonly ITransactionStatusManager _transactionStatusManager;

        public TransactionStatusController(ITransactionStatusManager transactionStatusManager)
        {
            _transactionStatusManager = transactionStatusManager;
        }

        /// <summary>
        /// Get transaction status list
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<TransactionStatusDto>))]
        public async Task<ActionResult<List<TransactionStatusDto>>> GetTransactionStatuses()
        {
            var result = await _transactionStatusManager.GetTransactionStatusesAsync();

            return Ok(result);
        }
    }
}
