﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ToCToP.TransactionHandler.Business.Managers;
using ToCToP.TransactionHandler.Models.Transaction.ViewModels;

namespace ToCToP.TransactionHandler.Api.Controllers
{
    public class TransactionController : BaseController
    {
        private readonly ITransactionManager _tranactionManager;

        public TransactionController(ITransactionManager transactionManager)
        {
            _tranactionManager = transactionManager;
        }

        /// <summary>
        /// Get transaction list
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<TransactionDto>))]
        public async Task<ActionResult<List<TransactionDto>>> GetTransactions()
        {
            var result = await _tranactionManager.GetTransactionsAsync();

            return Ok(result);
        }

        /// <summary>
        /// Get transactions by status
        /// </summary>
        /// <returns></returns>
        [HttpGet("{status}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<TransactionDto>))]
        public async Task<ActionResult<List<TransactionDto>>> GetTransactions(string status)
        {
            var result = await _tranactionManager.GetTransactionsAsync(status);

            return Ok(result);
        }

        /// <summary>
        /// Get transactions by status and currency code
        /// </summary>
        /// <returns></returns>
        [HttpGet("{status}/{currencyCode}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<TransactionDto>))]
        public async Task<ActionResult<List<TransactionDto>>> GetTransactions(string status,
                                                                              string currencyCode)
        {
            var result = await _tranactionManager.GetTransactionsAsync(status, currencyCode);

            return Ok(result);
        }

        /// <summary>
        /// Get transactions by status, currency code and date range
        /// </summary>
        /// <returns></returns>
        [HttpGet("{status}/{currencyCode}/{fromDate}/{toDate}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<TransactionDto>))]
        public async Task<ActionResult<List<TransactionDto>>> GetTransactions(string status,
                                                                              string currencyCode,
                                                                              DateTime fromDate,
                                                                              DateTime toDate)
        {
            var result = await _tranactionManager.GetTransactionsAsync(status, 
                                                                       currencyCode,
                                                                       fromDate,
                                                                       toDate);

            return Ok(result);
        }

        /// <summary>
        /// Import transactions data from flat file
        /// </summary>
        [HttpPost("import")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<IActionResult> ImportTransactions(IFormFile file)
        {
            await _tranactionManager.ImportTransactionsAsync(file);

            return NoContent();
        }
    }
}
