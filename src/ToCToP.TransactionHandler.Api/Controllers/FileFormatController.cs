﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using ToCToP.TransactionHandler.Business.Managers;
using ToCToP.TransactionHandler.Models.FileFormats.ViewModels;

namespace ToCToP.TransactionHandler.Api.Controllers
{
    public class FileFormatController : BaseController
    {
        private readonly IFileFormatManager _fileFormatManager;

        public FileFormatController(IFileFormatManager fileFormatManager)
        {
            _fileFormatManager = fileFormatManager;
        }

        /// <summary>
        /// Get file format list
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<FileFormatDto>))]
        public async Task<ActionResult<List<FileFormatDto>>> GetFileFormats()
        {
            var result = await _fileFormatManager.GetFileFormatsAsync();

            return Ok(result);
        }
    }
}
