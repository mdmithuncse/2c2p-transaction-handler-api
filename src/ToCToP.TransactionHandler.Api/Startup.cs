using AutoMapper;
using AutoMapper.EquivalencyExpression;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.OpenApi.Models;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using ToCToP.TransactionHandler.Api.Filter;
using ToCToP.TransactionHandler.Business.Managers;
using ToCToP.TransactionHandler.DataAccess.Context;
using ToCToP.TransactionHandler.DataAccess.Repositories.TransactionRepository;
using ToCToP.TransactionHandler.DataAccess.UnitOfWorks;
using ToCToP.TransactionHandler.Entities;
using ToCToP.TransactionHandler.Helpers;
using ToCToP.TransactionHandler.Models.Mapping;

namespace ToCToP.TransactionHandler.Api
{
    public class Startup
    {

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddLogging();
            services.AddCors();
            services.AddMemoryCache();
            services.AddHttpContextAccessor();

            services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("ToCToPDatabase")))
                    .AddUnitOfWork<ApplicationDbContext>()
                    .AddCustomRepository<Transaction, CustomTransactionRepository>();

            services.AddAutoMapper(options =>
            {
                options.AddCollectionMappers();
                options.AddProfile<MappingProfile>();
            }, Assembly.GetExecutingAssembly());

            services.AddTransient<IFileFormatManager, FileFormatManager>();
            services.AddTransient<ITransactionStatusManager, TransactionStatusManager>();
            services.AddTransient<ITransactionManager, TransactionManager>();

            services.AddTransient<ICustomTransactionRepository, CustomTransactionRepository>();

            services.AddTransient<ICsvFileBuilder, CsvFileBuilder>();

            services.AddRouting(options => options.LowercaseUrls = true);

            services.AddControllers(options =>
            {
                options.Filters.Add(typeof(ModelValidationFilter));
                options.Filters.Add(typeof(CustomExceptionFilter));
                options.ModelValidatorProviders.Clear();
            }).AddDataAnnotationsLocalization().AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
                options.JsonSerializerOptions.IgnoreNullValues = false;
                options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
            })
                    .AddFluentValidation(
                                         fv =>
                                         {
                                             fv.RegisterValidatorsFromAssembly(
                                                                               Assembly.Load(
                                                                                             Assembly.GetExecutingAssembly()
                                                                                                     .GetReferencedAssemblies()
                                                                                                     .FirstOrDefault(
                                                                                                                     assembly =>
                                                                                                                         assembly
                                                                                                                         .Name
                                                                                                                         .Equals(
                                                                                                                                 "ToCToP.TransactionHandler.Models")) ??
                                                                                             throw new InvalidOperationException()));
                                         });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("api", new OpenApiInfo 
                {
                    Version = "v1",
                    Title = "Transaction Handler Api",
                    Description = "A simple example of transaction handler ASP.NET Core Web API",
                    TermsOfService = new Uri("https://example.com/terms"),
                    Contact = new OpenApiContact
                    {
                        Name = "Md Nuruzzaman Mithun",
                        Email = string.Empty,
                        Url = new Uri("https://www.linkedin.com/in/md-nuruzzaman-mithun-06baa438/")
                    },
                    License = new OpenApiLicense
                    {
                        Name = "Use under LICX",
                        Url = new Uri("https://example.com/license"),
                    }
                });

                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(basePath, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IServiceProvider serviceProvider)
        {
            ConfigureAsync(app, env, serviceProvider).GetAwaiter().GetResult();
        }

        private async Task ConfigureAsync(IApplicationBuilder app, IWebHostEnvironment env, IServiceProvider serviceProvider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            else
            {
                app.UseExceptionHandler("/error");
                app.UseHsts();
            }

            var dbContext = serviceProvider.GetRequiredService<ApplicationDbContext>();
            await dbContext.DbMigrateAsync();

            app.UseCors();

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });

            app.UseSwagger();
            app.UseSwaggerUI(c => { c.SwaggerEndpoint("api/swagger.json", "Transaction Handler Api V1"); });
        }
    }
}
