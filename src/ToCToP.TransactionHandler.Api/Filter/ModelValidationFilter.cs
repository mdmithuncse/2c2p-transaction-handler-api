﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using ToCToP.TransactionHandler.Commons.Constants;
using ToCToP.TransactionHandler.Commons.Exceptions;

namespace ToCToP.TransactionHandler.Api.Filter
{
    /// <summary>
    ///     Model Validator Filter
    ///     Implementation responsable for the management of the input validation.
    ///     The customization applied regards the custom error structure raised every time.
    /// </summary>
    public class ModelValidationFilter : BaseFilter, IAsyncActionFilter
    {
        /// <inheritdoc />
        public ModelValidationFilter(ILogger<ModelValidationFilter> logger, IWebHostEnvironment env) : base(logger, env)
        {
        }

        #region OnActionExecuting

        private static T GetAttributeFrom<T>(Type type, string propertyName) where T : Attribute
        {
            var attrType = typeof(T);
            var property = type.GetProperty(propertyName);
            return (T) property?.GetCustomAttributes(attrType, false).FirstOrDefault();
        }

        /// <inheritdoc />
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var errors = new List<ApplicationError>();

            //Capture Serilaization error if any and return ServerError
            if (context.ModelState.ValidationState == ModelValidationState.Invalid)
            {
                foreach (var key in context.ModelState.Keys)
                {
                    var value = context.ModelState[key];
                    if (value.ValidationState == ModelValidationState.Invalid)
                    {
                        //track exceptions
                        foreach (var exception in value.Errors.Where(e => e.Exception != null).Select(e => e.Exception))
                        {
                            TrackException(context.HttpContext, exception);
                        }

                        errors.Add(new ApplicationError { Field = key, ErrorCode = ErrorCodes.SERIALIZATION_ERROR });
                    }
                }
            }

            //model validation
            foreach (var input in context.ActionArguments.ToList())
            {
                var validationContext = new ValidationContext(input.Value, null, null);
                var results = new List<ValidationResult>();
                if (!Validator.TryValidateObject(input.Value, validationContext, results, true))
                {
                    var type = input.Value.GetType();
                    foreach (var e in results)
                    {
                        var jsonProp = GetAttributeFrom<JsonPropertyAttribute>(type, e.MemberNames.FirstOrDefault());
                        errors.Add(new ApplicationError
                        {
                            Field = jsonProp?.PropertyName ?? e.MemberNames.FirstOrDefault(),
                            ErrorCode = ErrorCodes.SERIALIZATION_ERROR
                        });
                    }
                }
            }

            if (errors.Count > 0)
            {
                context.Result = new ObjectResult(errors) { StatusCode = 400, DeclaredType = typeof(ApplicationError[]) };
            }
            else
            {
                await next();
            }
        }

        #endregion OnActionExecuting
    }
}
