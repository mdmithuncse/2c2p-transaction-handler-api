﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using ToCToP.TransactionHandler.Commons.Constants;
using ToCToP.TransactionHandler.Commons.Exceptions;

namespace ToCToP.TransactionHandler.Api.Filter
{
    public class CustomExceptionFilter : BaseFilter, IAsyncExceptionFilter
    {
        public CustomExceptionFilter(ILogger<CustomExceptionFilter> logger, IWebHostEnvironment env) : base(logger, env)
        {

        }

        public Task OnExceptionAsync(ExceptionContext context)
        {
            var cqException = context.Exception as ApiApplicationException;

            var errorCode = cqException?.ErrorCode ?? ErrorCodes.GENERIC_ERROR;
            var field = cqException?.Field ?? string.Empty;
            var message = cqException?.Message ?? string.Empty;
            var applicationErrors = cqException?.ApplicationErrors ?? null;

            context.Result = new BadRequestObjectResult(new ApplicationError { ErrorCode = errorCode, Field = field, Message = message, applicationErrors = applicationErrors})
            {
                StatusCode = cqException?.ErrorCode,
                DeclaredType = typeof(ApplicationError)
            };

            return Task.CompletedTask;
        }
    }
}
