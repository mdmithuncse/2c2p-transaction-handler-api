﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using ToCToP.TransactionHandler.Commons.Exceptions;

namespace ToCToP.TransactionHandler.Api.Filter
{
    public abstract class BaseFilter : ResultFilterAttribute
    {
        private readonly IWebHostEnvironment _env;
        private readonly ILogger _logger;

        public BaseFilter(ILogger logger, IWebHostEnvironment env)
        {
            _logger = logger;
            _env = env;
        }

        public void TrackException(HttpContext context, Exception ex)
        {
            _logger.LogError(ex.HResult, ex, $"Unhandled exception. trace_id {context.TraceIdentifier}");
        }

        public ObjectResult ExceptionResult(HttpContext context, Exception ex)
        {
            return new ObjectResult(new ServerError { Trace = _env.IsProduction()
                ? "Unhandled server error"
                : ex.StackTrace,
                Message = _env.IsProduction()
                    ? "Unhandled server error"
                    : ex.Message,
                TraceId = context.TraceIdentifier }) { StatusCode = 500, DeclaredType = typeof(ServerError) };
        }

        public override void OnResultExecuting(ResultExecutingContext context)
        {
            base.OnResultExecuting(context);

            if (!context.ModelState.IsValid)
            {
                var errors = new List<ApplicationError>();
                foreach (var modelStateValue in context.ModelState.Values)
                {
                    var entry = modelStateValue;
                    var field = entry.GetType().GetTypeInfo().GetProperty("Key")?.GetValue(entry)?.ToString();
                    var message = entry.Errors.FirstOrDefault();
                    if (message != null)
                    {
                        errors.Add(new ApplicationError { ErrorCode = 400, Field = field });
                    }
                }

                if (errors.Any())
                {
                    context.Result = new ObjectResult(errors) { StatusCode = 400, DeclaredType = typeof(ApplicationError[]) };
                }
            }
        }
    }
}
