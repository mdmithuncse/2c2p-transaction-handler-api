﻿using System.Collections.Generic;

namespace ToCToP.TransactionHandler.Entities
{
    public class TransactionStatus : BaseEntity
    {
        public string Status { get; set; }

        public ICollection<Transaction> Transactions { get; set; }
    }
}
