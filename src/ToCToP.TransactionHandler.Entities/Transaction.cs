﻿using System;

namespace ToCToP.TransactionHandler.Entities
{
    public class Transaction : BaseEntity
    {
        public string TransactionId { get; set;}
        public decimal Amount { get; set; }
        public string CurrencyCode { get; set; }
        public DateTime Date { get; set; }
        public long TransactionStatusId { get; set; }
        public long FileFormatId { get; set; }

        public TransactionStatus TransactionStatus { get; set; }
        public FileFormat FileFormat { get; set; }
    }
}
