﻿using System;

namespace ToCToP.TransactionHandler.Entities
{
    public interface IBaseEntity
    {
        long Id { get; set; }

        DateTime Created { get; set; }

        DateTime? Updated { get; set; }
    }

    public interface IBaseCodeEntity
    {
        public string Code { get; set; }
    }
}
