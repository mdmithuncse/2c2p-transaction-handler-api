﻿using System.Collections.Generic;

namespace ToCToP.TransactionHandler.Entities
{
    public class FileFormat : BaseEntity
    {
        public string FileType { get; set; }

        public ICollection<Transaction> Transactions { get; set; }
    }
}
