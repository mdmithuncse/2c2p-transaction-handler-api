﻿using System;

namespace ToCToP.TransactionHandler.Entities
{
    public abstract class BaseCodeEntity : IBaseCodeEntity, IBaseEntity
    {
        public long Id { get; set; }

        public string Code { get; set; }

        public DateTime Created { get; set; }

        public DateTime? Updated { get; set; }

        public string CreatedBy { get; set; }

        public string UpdatedBy { get; set; }
    }
}
