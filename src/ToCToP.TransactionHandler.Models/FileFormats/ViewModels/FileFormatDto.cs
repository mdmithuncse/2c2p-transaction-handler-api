﻿using AutoMapper;
using ToCToP.TransactionHandler.Entities;
using ToCToP.TransactionHandler.Models.Mapping;

namespace ToCToP.TransactionHandler.Models.FileFormats.ViewModels
{
    public class FileFormatDto : IMapFrom<FileFormat>
    {
        public string FileType { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<FileFormat, FileFormatDto>();
        }
    }
}
