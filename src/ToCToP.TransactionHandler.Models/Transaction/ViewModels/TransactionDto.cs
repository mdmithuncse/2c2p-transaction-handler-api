﻿using AutoMapper;
using System;
using ToCToP.TransactionHandler.Models.Mapping;

namespace ToCToP.TransactionHandler.Models.Transaction.ViewModels
{
    public class TransactionDto : IMapFrom<Entities.Transaction>
    {
        public string TransactionId { get; set;}
        public decimal Amount { get; set; }
        public string CurrencyCode { get; set; }
        public DateTime Date { get; set; }

        public string Status { get; set; }
        
        public string FileType { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Entities.Transaction, TransactionDto>()
                   .ForMember(dest => dest.Status,
                              map => map.MapFrom(source => source.TransactionStatus.Status))
                   .ForMember(dest => dest.FileType,
                              map => map.MapFrom(source => source.FileFormat.FileType));
        }
    }
}
