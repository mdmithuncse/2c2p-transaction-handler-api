﻿namespace ToCToP.TransactionHandler.Models.Transaction.Input
{
    public class ImportTransactionCsvRequest
    {
        public string TransactionId { get; set; }
        public decimal Amount { get; set; }
        public string CurrencyCode { get; set; }
        public string Date { get; set; }
        public string Status { get; set; }
    }
}
