﻿using CsvHelper.Configuration;
using ToCToP.TransactionHandler.Commons.Constants;
using ToCToP.TransactionHandler.Models.Transaction.Input;

namespace ToCToP.TransactionHandler.Models.Transaction.Maps
{
    public class ImportTransactionCsvRequestMap : ClassMap<ImportTransactionCsvRequest>
    {
        public ImportTransactionCsvRequestMap()
        {
            Map(map => map.TransactionId).Index(0).Name(CsvFile.CsvFileHeaderNames.TRANSACTION_ID);
            Map(map => map.Amount).Index(1).Name(CsvFile.CsvFileHeaderNames.AMOUNT);
            Map(map => map.CurrencyCode).Index(2).Name(CsvFile.CsvFileHeaderNames.CURRENCY_CODE);
            Map(map => map.Date).Index(3).Name(CsvFile.CsvFileHeaderNames.DATE);
            Map(map => map.Status).Index(4).Name(CsvFile.CsvFileHeaderNames.STATUS);
        }
    }
}
