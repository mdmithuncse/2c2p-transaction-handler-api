﻿using AutoMapper;
using ToCToP.TransactionHandler.Models.Mapping;

namespace ToCToP.TransactionHandler.Models.TransactionStatus.ViewModels
{
    public class TransactionStatusDto : IMapFrom<Entities.TransactionStatus>
    {
        public string Status { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Entities.TransactionStatus, TransactionStatusDto>();
        }
    }
}