﻿using System;
using System.Collections.Generic;

namespace ToCToP.TransactionHandler.Commons.Exceptions
{
    public class ApiApplicationException : Exception
    {
        public ApiApplicationException(int httpStatusCode, 
                                       int errorCode, 
                                       string message = "Api Application Exception", 
                                       string field = null,
                                       List<ApplicationError> applicationErrors = null) : base(message)
        {
            HttpStatusCode = httpStatusCode;
            ErrorCode = errorCode;
            Field = field;
            ApplicationErrors = applicationErrors;
        }

        public int HttpStatusCode { get; set; }
        public int ErrorCode { get; set;}
        public string Field { get; set;}
        public List<ApplicationError> ApplicationErrors{ get; set;}
    }
}
