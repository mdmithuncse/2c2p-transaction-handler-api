﻿using System.Collections.Generic;

namespace ToCToP.TransactionHandler.Commons.Exceptions
{
    public class ApplicationError
    {
        public int ErrorCode { get; set; }
        public string Field { get; set; }
        public string Message { get; set; }
        public List<ApplicationError> applicationErrors { get; set;}
    }
}
