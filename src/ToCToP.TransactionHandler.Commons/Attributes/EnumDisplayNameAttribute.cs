﻿using System;

namespace ToCToP.TransactionHandler.Commons.Attributes
{
    public class EnumDisplayNameAttribute : Attribute
    {
        public string DisplayName { get; set; }
        public string DatabaseValue { get; set; }
    }
}
