﻿using ToCToP.TransactionHandler.Commons.Attributes;

namespace ToCToP.TransactionHandler.Commons
{
    public static class Enums
    {
        public struct TransactionStatus
        {
            public const string APPROVED = "Approved";
            public const string FAILED = "Failed";
            public const string FINISHED = "Finished";
            public const string REJECTED = "Rejected";
            public const string DONE = "Done";
        }

        public struct FileType
        {
            public const string CSV = "csv";
            public const string XML = "xml";
        }

        public enum FileFormat
        {
            [EnumDisplayName(DisplayName = "CSV", DatabaseValue = "1")] CSV = 1,
            [EnumDisplayName(DisplayName = "XML", DatabaseValue = "2")] XML = 2
        }

        public struct FileSize
        {
            public const int ONE_MB = 1000000;
        }
    }
}
