﻿namespace ToCToP.TransactionHandler.Commons.Constants
{
    public static class Delimiters
    {
        public const char CODE_DELIMITER = '|';
        public const char VALUE_DELIMITER = ',';
    }
}
