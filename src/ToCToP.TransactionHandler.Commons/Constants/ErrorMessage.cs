﻿namespace ToCToP.TransactionHandler.Commons.Constants
{
    public static class ErrorMessage
    {
        public const string NOT_FOUND = "data not found";

        public const string INVALID_REQUEST = "invalid request";

        public const string FILE_NOT_FOUND = "file not found";

        public const string TRANSACTION_NOT_FOUND = "transaction not found";
    }
}
