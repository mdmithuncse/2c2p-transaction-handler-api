﻿namespace ToCToP.TransactionHandler.Commons.Constants
{
    public class CsvFile
    {
        public struct CsvFileHeaderNames
        {
            public const string TRANSACTION_ID = "TransactionId";
            public const string AMOUNT = "Amount";
            public const string CURRENCY_CODE = "CurrencyCode";
            public const string DATE = "Date";
            public const string STATUS = "Status";
        }

        public struct DateFormats
        {
            public const string DD_MM_YYYY_HH_MM_SS = "dd/MM/yyyy HH:mm:ss";
        }
    }
}
