﻿namespace ToCToP.TransactionHandler.Commons.Constants
{
    public static class ErrorCodes
    {
        public const int GENERIC_ERROR = 1;
        public const int SERIALIZATION_ERROR = 2;

        public const int INVALID_REQUEST = 301;

        public const int BAD_REQUEST = 400;
        public const int FILE_NOT_FOUND = 401;
        public const int INVALID_FILE_FORMAT = 402;
        public const int INVALID_FILE_SIZE = 403;

        public const int TRANSACTION_NOT_FOUND = 501;
    }
}
