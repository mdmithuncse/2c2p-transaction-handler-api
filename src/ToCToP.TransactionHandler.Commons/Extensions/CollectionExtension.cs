﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ToCToP.TransactionHandler.Commons.Extensions
{
    public static class CollectionExtension
    {
        public static List<List<T>> ChunkBy<T>(this IEnumerable<T> source, int chunkSize)
        {
            return
                source.Select((x, i) => new { Index = i, Value = x })
                      .GroupBy(x => x.Index / chunkSize)
                      .Select(x => x.Select(v => v.Value).ToList())
                      .ToList();
        }

        public static IEnumerable<T> OrderBySequence<T, TId>(
            this IEnumerable<T> source,
            IEnumerable<TId> order,
            Func<T, TId> idSelector)
        {
            var lookup = source.ToLookup(idSelector, t => t);
            foreach (var id in order)
            {
                foreach (var t in lookup[id])
                {
                    yield return t;
                }
            }
        }
    }
}
