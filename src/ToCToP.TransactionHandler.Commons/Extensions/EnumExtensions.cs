﻿using System;
using System.ComponentModel;
using ToCToP.TransactionHandler.Commons.Attributes;

namespace ToCToP.TransactionHandler.Commons.Extensions
{
    public static class EnumExtensions
    {
        public static string DatabaseValue(this Enum value)
        {
            var field = value.GetType().GetField(value.ToString());

            var enumName =
                !(Attribute.GetCustomAttribute(field, typeof(EnumDisplayNameAttribute)) is EnumDisplayNameAttribute attribute)
                    ? value.ToString()
                    : string.IsNullOrEmpty(attribute.DatabaseValue)
                        ? attribute.DisplayName
                        : attribute.DatabaseValue;
            if (string.IsNullOrEmpty(enumName))
            {
                enumName = value.ToString();
            }

            return enumName;
        }

        public static string DisplayName(this Enum value)
        {
            var field = value.GetType().GetField(value.ToString());

            var enumName =
                !(Attribute.GetCustomAttribute(field, typeof(EnumDisplayNameAttribute)) is EnumDisplayNameAttribute attribute)
                    ? value.ToString()
                    : attribute.DisplayName;

            if (string.IsNullOrEmpty(enumName))
            {
                enumName = value.ToString();
            }

            return enumName;
        }

        public static T Enum<T>(this string value) where T : struct, Enum
        {
            var fields = typeof(T).GetFields();
            foreach (var field in fields)
            {
                if (Attribute.GetCustomAttribute(field, typeof(EnumDisplayNameAttribute)) is EnumDisplayNameAttribute attribute &&
                    attribute.DisplayName == value)
                {
                    return System.Enum.Parse<T>(field.Name);
                }

                if (field.Name.Equals(value))
                {
                    return System.Enum.Parse<T>(field.Name);
                }
            }

            throw new InvalidEnumArgumentException(value, -1, typeof(T));
        }
    }
}
