# 2C2P Transaction Data #

## Description ##

Technical Assignment for Software Architect

## Configuration and Installation ##

Please follow the below steps to run this project

* Clone the repository from master branch to your computer.
* Open the project in Visual Studio Enterprise 2019.
* Build the project. It'll automatically restore nuget packages.
* Change the connection string in appsettings.Local.json file.
* Select ToCToP.TransactionHandler.Api as startup project, if it's not selected.
* Run the project. It'll apply the database migrations and will start swagger UI.
* Now, you can check the API's from swagger UI.
* Enjoy!!!
