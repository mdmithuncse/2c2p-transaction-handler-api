FROM mcr.microsoft.com/dotnet/core/sdk:3.1

RUN mkdir -p /home/2c2p/transactionhandler \
	&& mkdir /home/2c2p/transactionhandler/api

WORKDIR /home/2c2p/transactionhandler

COPY . .

#
# Compile
#
RUN dotnet restore src/ToCToP.TransactionHandler.Api \ 
	&& dotnet publish src/ToCToP.TransactionHandler.Api -c Release -f netcoreapp3.1 -o /home/2c2p/transactionhandler/api

#
# Test
#
RUN dotnet test test/ToCToP.TransactionHandler.Business.Test

RUN dotnet test test/ToCToP.TransactionHandler.Services.Test

#
# Release
#
RUN chmod -R 777 /home/2c2p/transactionhandler/api

CMD cp -Rp /home/2c2p/transactionhandler/api/* /mnt